import json
from django.db.models import Sum, Q
from datetime import datetime
import os
import pandas as pd
from datetime import datetime

from . import models
from project.settings import MEDIA_ROOT

MONTHS_MAPPING = {
    'january': 'Январь', 'february': 'Февраль', 'march': 'Март', 'april': 'Апрель', 'may': 'Май', 'june': 'Июнь',
    'july': 'Июль', 'august': 'Август', 'september': 'Сентябрь', 'october': 'Октябрь', 'november': 'Ноябрь',
    'december': 'Декабрь'
}


class SortBudgetEntries:
    """Сортировка расходов и доходов за последний месяц. Вместо details_per_category()"""
    def __init__(self, user):
        self.user = user

    @property
    def last_aggr_budget(self):
        return models.AggregateBudget.objects.latest('created_at')

    @property
    def income_budget_entries(self):
        return self.last_aggr_budget.budget_entries.filter(type='i')

    @property
    def expense_budget_entries(self):
        return self.last_aggr_budget.budget_entries.filter(type='e')

    def get_unique_income_categories(self):
        return [i.category for i in self.income_budget_entries.distinct('category_id').order_by('category_id')]

    def get_unique_expense_categories(self):
        return [i.category for i in self.expense_budget_entries.distinct('category_id').order_by('category_id')]

    def get_income_sum(self):
        if not self.income_budget_entries:
            return 0
        else:
            return self.income_budget_entries.aggregate(Sum('amount'))['amount__sum']

    def get_expense_sum(self):
        if not self.expense_budget_entries:
            return 0
        else:
            return self.expense_budget_entries.aggregate(Sum('amount'))['amount__sum']

    def get_incomes_per_category(self):
        return {
            category.name: self.income_budget_entries.filter(category=category).aggregate(Sum('amount'))['amount__sum']
            for category in self.get_unique_income_categories()
        }

    def get_expenses_per_category(self):
        return {
            category.name: self.expense_budget_entries.filter(category=category).aggregate(Sum('amount'))['amount__sum']
            for category in self.get_unique_expense_categories()
        }

    def get_balance(self):
        return self.get_income_sum() - self.get_expense_sum()

    def get_context(self):

        try:
            context = {
                'total_income': self.get_income_sum(),
                'total_expense': self.get_expense_sum(),
                'expenses_per_category': self.get_expenses_per_category(),
                'incomes_per_category': self.get_incomes_per_category(),
                'balance': self.get_balance(),
            }
        except models.AggregateBudget.DoesNotExist:
            context = {
                'total_income': None,
                'total_expense': None,
                'expenses_per_category': None,
                'incomes_per_category': None,
                'balance': None,
            }
        return context


class AggrBudgetsToHistoryView:
    """Класс для сортировки агрегированных бюджетов по годам"""
    def __init__(self, user: int):
        self.user = user

    @property
    def aggr_budgets(self):
        return models.AggregateBudget.objects.filter(user_id=self.user)

    @property
    def distinct_years(self):
        years = self.aggr_budgets.values('year').distinct('year').order_by('year')
        return [i['year'] for i in years]

    @property
    def sorted_data(self):
        sorted_data = {year: list(self.aggr_budgets.filter(year=year)) for year in self.distinct_years}
        return sorted_data

    @property
    def ready_data(self):
        sorted_data = self.sorted_data
        for seq in sorted_data.values():
            for value in seq:
                setattr(value, 'balance', value.income_amount - value.expenses_amount)
                setattr(value, 'month', MONTHS_MAPPING[value.slug])
        return sorted_data


class ProcessingBudgetEntry:
    """Класс для обработки записи о бюджете. Реализует операции создания. изменения и удаления"""

    def __init__(self, instance) -> None:
        self.instance = instance

    def create(self) -> None:
        # тут обработка вновь создаваемой записи
        instance = self.instance

        instance.created_at = datetime.now()
        instance.day_num = instance.created_at.day
        instance.month_num = instance.created_at.month
        instance.month_str = instance.created_at.strftime('%B').lower()
        instance.year, instance.week_number, instance.day_of_week = instance.created_at.isocalendar()
        instance.type = instance.category.type

        try:
            instance.parent = models.AggregateBudget.objects.get(month=instance.month_num, year=instance.year,
                                                                 user=instance.user)
            instance.parent.expenses_amount += instance.amount if instance.type == 'e' else 0
            instance.parent.income_amount += instance.amount if instance.type == 'i' else 0
            instance.parent.save()
        except models.AggregateBudget.DoesNotExist:
            instance.parent = models.AggregateBudget.objects.create(
                month=instance.month_num,
                year=instance.year,
                income_amount=instance.amount if instance.type == 'i' else 0,
                expenses_amount=instance.amount if instance.type == 'e' else 0,
                slug=instance.month_str,
                user=instance.user
            )
        instance.save()

    def update(self) -> None:
        instance = self.instance

        old_instance = models.BudgetEntry.objects.get(id=instance.id)
        aggr_budget = old_instance.parent
        if old_instance.type == 'i':
            aggr_budget.income_amount -= old_instance.amount
            aggr_budget.income_amount += instance.amount
            aggr_budget.save()
        else:
            aggr_budget.expenses_amount -= old_instance.amount
            aggr_budget.expenses_amount += instance.amount
            aggr_budget.save()

        instance.updated_at = datetime.now()
        instance.changed = True
        instance.save()

    def delete(self) -> None:
        instance = self.instance
        parent = instance.parent

        if instance.type == 'i':
            parent.income_amount -= instance.amount
            parent.save()
            # return instance
        else:
            parent.expenses_amount -= instance.amount
            parent.save()
            # return instance
        instance.delete()


def details_per_category(user):
    """
    Функция считает общую сумму затрат и доходов за месяц.
    сумму затрат и доходов по категориям. НЕАКТУАЛЬНА, ЗАМЕНЕНА КЛАССОМ SortBudgetEntries
    """

    try:
        # Получить последний агрегированный бюджет пользователя
        last_month_budget = models.AggregateBudget.objects.filter(user=user).first()

        # Сортировка BudgetEntries на доход/расход
        income_budget_entries = last_month_budget.budget_entries.filter(type='i')
        expense_budget_entries = last_month_budget.budget_entries.filter(type='e')

        # Получить уникальные категории для дохода/расхода
        income_categories = [
            i.category for i in
            income_budget_entries.distinct('category_id').order_by('category_id')
        ]
        expense_categories = [
            i.category for i in
            expense_budget_entries.distinct('category_id').order_by('category_id')
        ]

        # Посчитать общие суммы дохода/расхода
        if not income_budget_entries:
            total_income = {'amount__sum': 0}
        else:
            total_income = income_budget_entries.aggregate(Sum('amount'))
        if not expense_budget_entries:
            total_expense = {'amount__sum': 0}
        else:
            total_expense = expense_budget_entries.aggregate(Sum('amount'))

        # Посчитать суммы по каждой категории в отдельности
        expenses_per_category = {
            category.name: expense_budget_entries.filter(category=category)
            .aggregate(Sum('amount'))['amount__sum']
            for category in expense_categories
        }
        incomes_per_category = {
            category.name: income_budget_entries.filter(category=category)
            .aggregate(Sum('amount'))['amount__sum']
            for category in income_categories
        }

        balance = total_income['amount__sum'] - total_expense['amount__sum']
        context = {
            'total_income': total_income,
            'total_expense': total_expense,
            'expenses_per_category': expenses_per_category,
            'incomes_per_category': incomes_per_category,
            'balance': balance,
        }
        return context
    except AttributeError:
        return {}


def aggregate_budgets_per_year(user):
    """НЕАКТУАЛЬНО"""
    total_data_per_years = {}
    all_aggregate_budgets = models.AggregateBudget.objects.filter(user=user)
    years = all_aggregate_budgets.values('year').distinct('year').order_by('year')
    for i in years:
        year_data = all_aggregate_budgets.filter(year=i['year'])

        total_data_per_years.update({i['year']: list(year_data)})
    return total_data_per_years


def process_budget_entry(instance, delete=None):
    """НЕАКТУАЛЬНО"""

    if delete:
        parent = instance.parent
        if instance.type == 'i':
            parent.income_amount -= instance.amount
            parent.save()
            return instance
        else:
            parent.expenses_amount -= instance.amount
            parent.save()
            return instance

    if not instance.changed:
        # тут обработка вновь создаваемой записи
        instance.created_at = datetime.now()
        instance.day_num = instance.created_at.day
        instance.month_num = instance.created_at.month
        instance.month_str = instance.created_at.strftime('%B').lower()
        instance.year, instance.week_number, instance.day_of_week = instance.created_at.isocalendar()
        instance.type = instance.category.type

        try:
            instance.parent = models.AggregateBudget.objects.get(month=instance.month_num, year=instance.year,
                                                          user=instance.user)
            instance.parent.expenses_amount += instance.amount if instance.type == 'e' else 0
            instance.parent.income_amount += instance.amount if instance.type == 'i' else 0
            instance.parent.save()
        except models.AggregateBudget.DoesNotExist:
            instance.parent = models.AggregateBudget.objects.create(
                month=instance.month_num,
                year=instance.year,
                income_amount=instance.amount if instance.type == 'i' else 0,
                expenses_amount=instance.amount if instance.type == 'e' else 0,
                slug=instance.month_str,
                user=instance.user
            )
        return instance
    else:
        # тут обработка изменения записи
        old_instance = models.BudgetEntry.objects.get(id=instance.id)
        aggr_budget = old_instance.parent
        if old_instance.type == 'i':
            aggr_budget.income_amount -= old_instance.amount
            aggr_budget.income_amount += instance.amount
            aggr_budget.save()
        else:
            aggr_budget.expenses_amount -= old_instance.amount
            aggr_budget.expenses_amount += instance.amount
            aggr_budget.save()
        return instance


def to_dict(budget_entry):
    fields = ['title', 'type', 'amount', 'created_at']
    data = {i: str(getattr(budget_entry, i)) for i in fields}
    data['type'] = 'расход' if data['type'] == 'e' else 'доход'
    return data


def generate_excel(json_data, user, month, year):
    filename = f'{user}_{month}_{year}.xlsx'
    file_dir = os.path.join(MEDIA_ROOT, filename)
    writer = pd.ExcelWriter(file_dir, engine='xlsxwriter')
    df = pd.read_json(json.dumps(json_data, ensure_ascii=False))
    df.to_excel(writer, 'Sheet1')
    writer.save()
    return file_dir


def get_budget_entries(aggr_budget):
    return aggr_budget.budget_entries.all()


def get_aggr_budget(pk):
    return models.AggregateBudget.objects.get(id=pk)


def filter_budget_entries(aggr_budget, type):
    budget_entries = aggr_budget.budget_entries.all()
    q_type = Q(type__in=type)
    queryset = budget_entries.filter(q_type)
    return queryset


def get_budget_entry(pk):
    return models.BudgetEntry.objects.get(id=pk)
