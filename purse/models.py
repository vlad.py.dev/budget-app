from django.db import models
from uuid import uuid4
from pytils.translit import slugify
from users.models import CustomUser
from datetime import datetime
from django.urls import reverse


# Create your models here.
class BudgetCategory(models.Model):
    """Базовая модель категории расходов"""
    TYPES = (
        ('i', 'Доход'),
        ('e', 'Расход')
    )

    type = models.CharField(max_length=1, choices=TYPES, verbose_name='Тип', null=True, blank=True)
    name = models.CharField(max_length=155, verbose_name='Категория', unique=True)
    description = models.CharField(max_length=255, verbose_name='Описание', null=True, blank=True)
    slug = models.SlugField(max_length=155, unique=True)
    super_category = models.ForeignKey('SuperCategory', on_delete=models.PROTECT, null=True, blank=True, related_name='sub_categories', verbose_name='Надкатегория')

    def save(self, commit=False, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(BudgetCategory, self).save(*args, **kwargs)


class SuperCategoryManager(models.Manager):
    """Менеджер надкатегорий"""
    def get_queryset(self):
        return super().get_queryset().filter(super_category__isnull=True)


class SuperCategory(BudgetCategory):
    """Модель надкатегории"""
    objects = SuperCategoryManager()

    def __str__(self):
        return self.name

    class Meta:
        proxy = True
        verbose_name = 'Надкатегория'
        verbose_name_plural = 'Надкатегории'


class SubCategoryManager(models.Manager):
    """Менеджер подкатегории"""
    def get_queryset(self):
        return super().get_queryset().filter(super_category__isnull=False)


class SubCategory(BudgetCategory):
    """Модель подкатегории"""
    objects = SubCategoryManager()

    def __str__(self):
        return self.name

    class Meta:
        proxy = True
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'


class AggregateBudget(models.Model):
    """Аггрегированный бюджет за месяц"""

    id = models.UUIDField(unique=True, default=uuid4, primary_key=True, db_index=True, verbose_name='id')
    month = models.PositiveSmallIntegerField(verbose_name='Месяц')
    year = models.PositiveSmallIntegerField(verbose_name='Год')
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name='Пользователь')
    income_amount = models.PositiveIntegerField(verbose_name='Сумма дохода', default=0)
    expenses_amount = models.PositiveIntegerField(verbose_name='Сумма расхода', default=0)
    slug = models.SlugField(max_length=155)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    class Meta:
        verbose_name = 'Агрегированный бюджет за месяц'
        verbose_name_plural = 'Агрегированные бюджеты за месяц'
        ordering = ['created_at']

    def get_absolute_url(self):
        user = self.user
        return reverse('purse:budget_detail', kwargs={'user': user, 'year': self.year, 'slug': self.slug})

    def __str__(self):
        return f'Бюджет за {self.slug} {self.year} года'


class BudgetEntry(models.Model):
    """Запись о доходе/расходе"""

    id = models.UUIDField(unique=True, default=uuid4, primary_key=True, db_index=True, verbose_name='id')
    title = models.CharField(max_length=155, verbose_name='Заголовок')
    type = models.CharField(max_length=1, verbose_name='Тип')
    category = models.ForeignKey(SubCategory, on_delete=models.CASCADE, verbose_name='Категория')
    amount = models.PositiveIntegerField(verbose_name='Сумма')

    day_num = models.PositiveIntegerField(verbose_name='Число', blank=True)
    month_num = models.PositiveIntegerField(verbose_name='Месяц число', blank=True)
    month_str = models.CharField(max_length=15, blank=True, verbose_name='Месяц строка')
    year = models.PositiveIntegerField(verbose_name='Год', blank=True)
    week_number = models.PositiveIntegerField(verbose_name='Номер недели', blank=True)
    day_of_week = models.CharField(max_length=15, blank=True, verbose_name='День недели')

    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name='Пользователь', blank=True)
    parent = models.ForeignKey(AggregateBudget, on_delete=models.CASCADE, verbose_name='Агрегированный бюджет', blank=True, related_name='budget_entries')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания', blank=True)
    updated_at = models.DateTimeField(verbose_name='Дата обновления', blank=True, null=True)
    changed = models.BooleanField(default=False, verbose_name='Изменялась')

    class Meta:
        verbose_name = 'Запись о расходе/доходе'
        verbose_name_plural = 'Записи о расходе/доходе'
        ordering = ['created_at']

    def __str__(self):
        return f'{self.user} - {self.id}'
