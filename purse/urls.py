from django.urls import path
from . import views


app_name = 'purse'

urlpatterns = [
    path('', views.IndexPageView.as_view(), name='index'),  # переделал
    # CRUD
    path('profile/add_operations/', views.AddBudgetEntryView.as_view(), name='add_operations'),  # create
    path('create_expense/', views.AddBudgetEntryExpenseView.as_view(), name='add_expense'),  # create
    path('create_income/', views.AddBudgetEntryIncomeView.as_view(), name='add_income'),  # create
    path('profile/<str:user>/<int:year>/<str:slug>/', views.AggrBudgetDetailView.as_view(), name='budget_detail'),  # read
    path('profile/history/', views.HistoryOperationsView.as_view(), name='history'),  # read
    path('profile/change/<str:pk>/', views.BudgetEntryUpdateView.as_view(), name='change_budget_entry'),  # update
    path('profile/delete_budget_entry/<str:pk>/', views.DeleteBudgetEntryView.as_view(), name='delete_budget_entry'),  # delete
    path('profile/delete_aggr_budget/<str:pk>/', views.DeleteAggregateBudgetView.as_view(), name='delete_aggr_budget'),  # delete
    # charts
    path('profile/charts/', views.ChartsView.as_view(), name='charts'),
    # export data
    path('profile/download/<str:pk>/', views.download, name='download'),
    path('profile/filter/', views.FilterBudgetEntriesView.as_view(), name='filter'),
]
