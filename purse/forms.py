from django import forms


from .models import BudgetEntry, SubCategory


class BudgetEntryExpenseForm(forms.ModelForm):
    """Форма для создания записи о доходе/расходе"""
    category = forms.ModelChoiceField(queryset=SubCategory.objects.filter(type='e'), label='Категория')

    class Meta:
        model = BudgetEntry
        fields = ['title', 'type', 'category', 'amount', 'user']
        widgets = {
            'user': forms.HiddenInput,
            'title': forms.TextInput(attrs={'class': 'form-control form-control-sm'}),
            'type': forms.HiddenInput,
            'amount': forms.TextInput(attrs={'class': 'form-control form-control-sm', 'type': 'number'})
        }


class BudgetEntryIncomeForm(forms.ModelForm):
    """Форма для создания записи о доходе/расходе"""
    category = forms.ModelChoiceField(queryset=SubCategory.objects.filter(type='i'), label='Категория')

    class Meta:
        model = BudgetEntry
        fields = ['title', 'type', 'category', 'amount', 'user']
        widgets = {
            'user': forms.HiddenInput,
            'title': forms.TextInput(attrs={'class': 'form-control form-control-sm'}),
            'type': forms.HiddenInput,
            'amount': forms.TextInput(attrs={'class': 'form-control form-control-sm', 'type': 'number'})
        }


class BudgetEntryChangeForm(forms.ModelForm):
    """Форма для изменения записи о доходе/расходе"""

    class Meta:
        model = BudgetEntry
        fields = ['title', 'amount', 'user', 'type']
        widgets = {
            'user': forms.HiddenInput,
            'title': forms.TextInput(attrs={'class': 'form-control form-control-sm'}),
            'amount': forms.TextInput(attrs={'class': 'form-control form-control-sm', 'type': 'number'}),
            'type': forms.HiddenInput
        }
