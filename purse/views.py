from django.http import FileResponse
from django.urls import reverse
from django.shortcuts import redirect, render
from django.views.generic.base import TemplateView, View
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages


from . import models
from . import forms
from . import services
from users.forms import CustomUserLoginForm


# Create your views here.
class IndexPageView(TemplateView):
    """Индексная страница, доступна всем"""
    template_name = 'purse/index_page.html'
    extra_context = {'login_form': CustomUserLoginForm, 'section': 'index'}


class AddBudgetEntryView(LoginRequiredMixin, View):
    """Класс для отображения форм для добавления записи о расходе/доходе"""
    def get(self, request):
        form_income = forms.BudgetEntryIncomeForm(self.request.GET or None, initial={'type': 'i', 'user': self.request.user})
        form_expense = forms.BudgetEntryExpenseForm(self.request.GET or None,
                                              initial={'type': 'e', 'user': self.request.user})
        context = {'form_income': form_income, 'form_expense': form_expense, 'section': 'add_operations'}
        context.update(services.SortBudgetEntries(request.user).get_context())
        return render(request, 'purse/add_operations.html', context)


class AddBudgetEntryExpenseView(LoginRequiredMixin, View):
    """Класс для создания записи расхода"""

    def post(self, request):
        form = forms.BudgetEntryExpenseForm(request.POST)
        if form.is_valid():
            new_instance = form.save(commit=False)
            services.ProcessingBudgetEntry(new_instance).create()
            messages.add_message(request, messages.SUCCESS, 'Запись создана')
            return redirect('purse:add_operations')


class AddBudgetEntryIncomeView(LoginRequiredMixin, View):
    """Класс для создания записи дохода"""

    def post(self, request):
        form = forms.BudgetEntryIncomeForm(request.POST)
        if form.is_valid():
            new_instance = form.save(commit=False)
            services.ProcessingBudgetEntry(new_instance).create()
            messages.add_message(request, messages.SUCCESS, 'Запись создана')
            return redirect('purse:add_operations')


class HistoryOperationsView(LoginRequiredMixin, View):
    """Отображение агрегированной истории операция по месяцам"""

    def get(self, request):
        data = services.AggrBudgetsToHistoryView(request.user.pk)
        context = {'data': data.ready_data, 'section': 'history'}
        return render(request, 'purse/history.html', context)


class BudgetEntryUpdateView(LoginRequiredMixin, View):
    """Класс для изменения записи о расходе/доходе"""
    old_instance = None

    def dispatch(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        self.old_instance = services.get_budget_entry(pk)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk):
        form = forms.BudgetEntryChangeForm(instance=self.old_instance)
        context = {'form': form, 'category': self.old_instance.category, 'back_url': self.old_instance.parent.get_absolute_url()}
        return render(request, 'purse/change_budget_entry.html', context)

    def post(self, request, pk):
        new_instance = forms.BudgetEntryChangeForm(request.POST, instance=self.old_instance)

        if new_instance.is_valid():
            new_instance = new_instance.save(commit=False)
            services.ProcessingBudgetEntry(new_instance).update()
            messages.add_message(request, messages.SUCCESS, 'Запись обновлена')
            return redirect(reverse('purse:budget_detail', kwargs={'user': self.old_instance.parent.user,
                                                                   'year': self.old_instance.parent.year,
                                                                   'slug': self.old_instance.parent.slug}))
        else:
            return redirect('purse:history')


class DeleteBudgetEntryView(LoginRequiredMixin, View):
    """Класс для удаления записи о доходе/расходе"""
    old_instance = None

    def dispatch(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        self.old_instance = services.get_budget_entry(pk)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk):
        instance = services.get_budget_entry(pk)
        services.ProcessingBudgetEntry(instance).delete()
        return redirect(reverse('purse:budget_detail', kwargs={'user': self.old_instance.parent.user,
                                                               'year': self.old_instance.parent.year,
                                                               'slug': self.old_instance.parent.slug}))


class DeleteAggregateBudgetView(LoginRequiredMixin, View):
    """Класс для удаления агрегированного бюджета за месяц"""
    old_instance = None

    def dispatch(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        self.old_instance = services.get_aggr_budget(pk)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk):
        aggr_budget = self.old_instance
        self.old_instance.delete()
        return redirect('purse:history')


class TypeCategory:
    def get_types(self):
        return models.BudgetEntry.objects.distinct('type').order_by('type').values('type')

    def get_categories(self):
        return models.SubCategory.objects.distinct('name').order_by('name').values('name')


class AggrBudgetDetailView(TypeCategory, LoginRequiredMixin, ListView):
    template_name = 'purse/budget_detail.html'
    extra_context = {'section': 'history'}
    paginate_by = 6

    def get_queryset(self):
        return models.BudgetEntry.objects.filter(
            user=self.request.user.pk,
            year=self.kwargs['year'],
            month_str=self.kwargs['slug']
        )


class FilterBudgetEntriesView(TypeCategory, LoginRequiredMixin, ListView):
    """Класс для фильтра записей о доходе/расходе"""
    template_name = 'purse/budget_detail.html'
    extra_context = {'section': 'history'}
    paginate_by = 6

    def get_queryset(self):
        queryset = models.BudgetEntry.objects.filter(user=self.request.user, type__in=self.request.GET.getlist('type'))
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        return context


class ChartsView(LoginRequiredMixin, View):
    """Графики"""
    def get(self, request, *args, **kwargs):
        return render(request, 'purse/charts.html', {'section': 'charts'})


def download(request, pk):
    aggr_budget = models.AggregateBudget.objects.get(id=pk)
    user = aggr_budget.user
    month = aggr_budget.slug
    year = aggr_budget.year
    budget_entries = list(aggr_budget.budget_entries.all())
    data = [services.to_dict(i) for i in budget_entries]
    file_dir = services.generate_excel(data, user, month, year)
    file = open(file_dir, 'rb')
    return FileResponse(file)
