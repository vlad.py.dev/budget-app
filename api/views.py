from rest_framework import status
from django.http import JsonResponse
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import TokenError, InvalidToken
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework.views import APIView


from purse import models as purse_models
from . import serializers


# Create your views here.
from .serializers import AggregateBudgetSerializer


class CustomTokenViewBase(TokenObtainPairView):
    """Изменен класс ответа на JsonResponse"""
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        return JsonResponse(serializer.validated_data, status=status.HTTP_200_OK, safe=False)


class CustomTokenRefreshView(TokenRefreshView):
    """Изменен класс ответа на JsonResponse"""
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        return JsonResponse(serializer.validated_data, status=status.HTTP_200_OK, safe=False)


class BudgetEntryView(APIView):
    """Вывод записей о расходе/доходе"""
    def get(self, request):
        data = purse_models.BudgetEntry.objects.all()
        serializer = serializers.BudgetEntrySerializer(data, many=True)
        return JsonResponse(serializer.data, safe=False)


class AggregateBudgetView(APIView):
    """Вывод агрегированных бюджетов"""
    def get(self, request):
        data = purse_models.AggregateBudget.objects.all()
        serializer = serializers.AggregateBudgetSerializer(data, many=True)
        return JsonResponse(serializer.data, safe=False)


class ChartView(ListAPIView):
    queryset = purse_models.AggregateBudget.objects.all()
    serializer_class = AggregateBudgetSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(user=self.request.user.id)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
