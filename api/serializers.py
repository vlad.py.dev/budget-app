from rest_framework import serializers


from purse import models as purse_models
from users import models as users_models


class SubCategorySerializer(serializers.ModelSerializer):
    """Сериализотор для вывода категорий"""

    class Meta:
        model = purse_models.SubCategory
        fields = ['name']


class BudgetEntrySerializer(serializers.ModelSerializer):
    """Сериализатор для записи о расходе/доходе"""
    # category = SubCategorySerializer()
    category = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = purse_models.BudgetEntry
        exclude = ['id', 'created_at', 'updated_at', 'changed', 'user', 'parent']


class AggregateBudgetSerializer(serializers.ModelSerializer):
    """Сериализатор для агрегированных бюджетов"""
    budget_entries = BudgetEntrySerializer(many=True)
    user = serializers.SlugRelatedField(slug_field='username', read_only=True)

    class Meta:
        model = purse_models.AggregateBudget
        fields = ['user', 'month', 'year', 'income_amount', 'expenses_amount', 'budget_entries']
