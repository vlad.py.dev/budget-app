from django.urls import path
from . import views

app_name = 'api'
urlpatterns = [
    # jwt auth
    path('auth/token-pair/', views.CustomTokenViewBase.as_view(), name='token_pair'),
    path('auth/token-refresh/', views.CustomTokenRefreshView.as_view(), name='token_refresh'),
    # views
    path('budget_entries/', views.BudgetEntryView.as_view(), name='budget_entries'),
    path('aggr_budgets/', views.AggregateBudgetView.as_view(), name='aggr_budgets'),
    path('charts/', views.ChartView.as_view(), name='charts-api')
]
