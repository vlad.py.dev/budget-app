from django.contrib.auth.backends import ModelBackend, UserModel
from django.db.models import Q


class EmailAuthBackend(ModelBackend):
    """Бэк авторизации по почте"""
    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            q = Q(username__iexact=username) | Q(email__iexact=username)
            user = UserModel.objects.get(q)
        except UserModel.DoesNotExist:
            return None
        else:
            if user.check_password(password) and self.user_can_authenticate(user):
                return user

    def get_user(self, user_id):
        try:
            user = UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None
        return user if self.user_can_authenticate(user) else None
