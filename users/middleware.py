from social_django.middleware import SocialAuthExceptionMiddleware
from django.contrib.messages.api import warning
from django.shortcuts import redirect
import re
from django.db.utils import IntegrityError


class CustomSocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):

    def process_exception(self, request, exception):
        url_exc = request.get_full_path_info()
        if url_exc.startswith('/social'):
            if isinstance(exception, IntegrityError):
                pattern = re.compile(r'([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}')
                exc_message = self.get_message(request, exception)
                url = self.get_redirect_uri(request, exception)
                res = re.search(pattern, exc_message)
                email_start_index = res.start()
                email_end_index = res.end()
                email = exc_message[email_start_index:email_end_index]
                message = f'Пользователь с адресом электронной почты {email} уже зарегистрирован на сайте'
                warning(request, message)
                return redirect(url)
            else:
                exc_message = self.get_message(request, exception)
                url = self.get_redirect_uri(request, exception)
                warning(request, exc_message)
                return redirect(url)
        else:
            pass
