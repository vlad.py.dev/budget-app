from django.contrib.auth.models import BaseUserManager


class CustomUserManager(BaseUserManager):

    def create_user(self, username, password=None, **kwargs):
        if not username:
            raise ValueError('Please, type your username')

        email = kwargs.get('email', None)
        if email:
            user = self.model(username=username, email=email)
        else:
            user = self.model(username=username)

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password=None):
        user = self.create_user(username=username, password=password)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


# pattern = '   8  )(961)-586 12 25   '
# pattern = pattern.strip()
# print(pattern)
# spaces = True
# while spaces:
#     if ' ' in pattern:
#         pattern = pattern.replace(' ', '')
#     elif '-' in pattern:
#         pattern = pattern.replace('-', '')
#     elif '(' in pattern:
#         pattern = pattern.replace('(', '')
#     elif ')' in pattern:
#         pattern = pattern.replace(')', '')
#     else:
#         spaces = False
# print(pattern)
