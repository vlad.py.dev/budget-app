//// AJAX экспорта в excel
var exportLink = document.getElementById('exportToExcel');

exportLink.addEventListener('click', function(event) {
    const request = new XMLHttpRequest();
    const url = exportLink.value;
    request.open('GET', url);
    request.addEventListener('readystatechange', () => {
        if (request.readyState == 4 && request.status == 200) {
            response = JSON.parse(request.responseText);
            response = response['details']
            var myJson = JSON.stringify(response, null, 4);
            var downloadLink = document.createElement("a");
            var file = new Blob([myJson], {type: 'application/json;'});
            downloadLink.href = URL.createObjectURL(file);
            downloadLink.download = 'example.json';
            downloadLink.click();
        }
    })
    request.send()
})


